<?php declare(strict_types=1);

namespace JFrame\Theme\JFrame\Configuration;

interface ThemeJFrameConfigurationKeys
{
    public const FOOTER_COPY    = 'jtheme_jframe_footer_copy';
    public const JQUERY_CDN     = 'jtheme_jframe_jquery_cdn';
    public const SCHEMA_NAME    = 'theming_schema_name';
    public const SCHEMA_PATH    = 'theming_schema_path';
    public const TPL_PATH       = 'theming_tpl_path';
    public const ADDITIONAL_CSS = 'theming_add_css';
}
