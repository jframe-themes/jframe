<?php declare(strict_types=1);

use JFrame\Configuration\Dto\Configuration;
use JFrame\Sites\SitesManager;
use JFrame\Theme\JFrame\Configuration\ThemeJFrameConfigurationKeys;

return [
    ThemeJFrameConfigurationKeys::FOOTER_COPY => (new Configuration())
        ->setKey(ThemeJFrameConfigurationKeys::FOOTER_COPY)
        ->setValue('J•Frame &copy; since 2008 by <a href="mailto:Jan_Doll@jfra.me">Jan_Doll@jfra.me</a>')
        ->setOwner('theming')
        ->setType('config')
        ->setSiteKey(SitesManager::MAIN_SITE_KEY),

    ThemeJFrameConfigurationKeys::JQUERY_CDN => (new Configuration())
        ->setKey(ThemeJFrameConfigurationKeys::JQUERY_CDN)
        ->setValue(rtrim(JF_BASE_URL,'/') . '/composer/vendor/components/jquery/jquery.min.js')
        ->setOwner('theming')
        ->setType('config')
        ->setSiteKey(SitesManager::MAIN_SITE_KEY),

    ThemeJFrameConfigurationKeys::SCHEMA_NAME => (new Configuration())
        ->setKey(ThemeJFrameConfigurationKeys::SCHEMA_NAME)
        ->setValue('dark')
        ->setOwner('theming')
        ->setType('config')
        ->setSiteKey(SitesManager::MAIN_SITE_KEY),

    ThemeJFrameConfigurationKeys::SCHEMA_PATH => (new Configuration())
        ->setKey(ThemeJFrameConfigurationKeys::SCHEMA_PATH)
        ->setValue(rtrim(JF_THEMES_DIR,DS) . DS . 'jframe' . DS . 'skin' . DS . 'less' . DS . 'variables' . DS . 'dark')
        ->setOwner('theming')
        ->setType('config')
        ->setSiteKey(SitesManager::MAIN_SITE_KEY),

    ThemeJFrameConfigurationKeys::TPL_PATH => (new Configuration())
        ->setKey(ThemeJFrameConfigurationKeys::TPL_PATH)
        ->setValue(rtrim(JF_THEMES_DIR,DS) . DS . 'jframe' . DS . 'tpl')
        ->setOwner('theming')
        ->setType('config')
        ->setSiteKey(SitesManager::MAIN_SITE_KEY),

    ThemeJFrameConfigurationKeys::ADDITIONAL_CSS => (new Configuration())
        ->setKey(ThemeJFrameConfigurationKeys::ADDITIONAL_CSS)
        ->setValue('')
        ->setOwner('theming')
        ->setType('config')
        ->setSiteKey(SitesManager::MAIN_SITE_KEY),
];
