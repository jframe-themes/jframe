<?php
/**
 * Header Template File for Header Section
 *
 * @project J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright 2008-2017 by Jan Doll
 */

use Composer\InstalledVersions;
use JFrame\Core\Configuration\CoreConfigurationKeys as CoreConfigKey;
use JFrame\Theming\ThemeGlobals;

/** @var ThemeGlobals $globals */

$currLangKey = ($globals->getLanguage()->getCode() !== $globals->getDefaultLanguage()->getCode()) ? $globals->getLanguage()->getCode() . '/' : '';
$currSiteKey = ($globals->getSite()->getKey() !== $globals->getMainSite()->getKey()) ? $globals->getSite()->getKey() : '';

$logoUrl = $globals->getBaseUrl() . $currLangKey . $currSiteKey;

try {
    $projectName = $globals->getConfig()->get(CoreConfigKey::PROJECT_NAME);
} catch (\JFrame\Configuration\Exception\ConfigurationNotFound $e) {
    $projectName = 'J&bull;Frame';
}

$userMenu = $globals->get('user-menu') ?? '';
//$langChooser = $Core->i18n()->getLangChooser();
$langChooser = '';

$coreVersion = (in_array('jframe/core', InstalledVersions::getInstalledPackages(), true)) ? InstalledVersions::getPrettyVersion('jframe/core') : 'n/A';
$jframeThemeVersion = (in_array('jframe/jframe-theme', InstalledVersions::getInstalledPackages(), true)) ? InstalledVersions::getPrettyVersion('jframe/jframe-theme') : 'n/A';
?>
<header id="header">
    <div class="container">
        <div class="row">
            <div class="logo col-sm-3">
                <a class="logo-link" href="<?php echo $logoUrl; ?>">
<!--                    <img class="img-responsive" src="--><?php //echo $Mvc->getThemeSkinUri(); ?><!--/img/logo.png" alt="--><?php //echo $Mvc->getMetaTitle(); ?><!--">-->
                    <span><?php echo $projectName; ?></span>
                </a>
            </div>

            <div class="lang-chooser-wrapper center-vertical col-sm-9">
                <div class="row">

                    <?php $helpSupportColClass = 'col-sm-6'; ?>
                    <?php if($userMenu !== ''): ?>
                        <div class="col-xs-6">
                            <div class="user-menu-wrapper visible-xs">
                                <ul class="list-unstyled">
                                    <?php echo $userMenu; ?>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-12">
                            <div class="lang-chooser text-right">
                                <?php // echo $Core->i18n()->getLangChooser(); ?>
                            </div>
                        </div>
                        <?php $helpSupportColClass = 'col-sm-12'; ?>
                    <?php endif; ?>

                    <?php if($langChooser === ''): ?>
                        <?php $helpSupportColClass = 'col-sm-12'; ?>
                    <?php endif; ?>

                    <div class="<?php echo $helpSupportColClass; ?>">
                        <div class="help-support text-right">
                            <a href="https://wiki.jfra.me/" target="_blank" title="J•Frame - Wiki" data-toggle="tooltip" data-placement="bottom">
                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                <span class="hidden-xs">
                                    Wiki
                                </span>
                            </a>
                            |
                            <a href="https://susy.jfra.me/" target="_blank" title="J•Frame - Support" data-toggle="tooltip" data-placement="bottom">
                                <i class="fa fa-life-ring" aria-hidden="true"></i>
                                <span class="hidden-xs">
                                    Support
                                </span>
                            </a>
                            |
                            <a href="#" tabindex="0"
                                title="J•Frame - Info"
                                data-content="Core v<?php echo $coreVersion; ?><br />Theme v<?php echo $jframeThemeVersion; ?>"

                                data-toggle="popover"
                                data-placement="bottom"
                                data-trigger="focus"
                                data-container="body"
                            >
                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                                <span class="hidden-xs">
                                    Info
                                </span>
                            </a>
                        </div>
                    </div>

                    <?php if($userMenu === ''): ?>
                        <div class="col-xs-6">
                            <div class="lang-chooser text-right">
                                <?php echo $langChooser; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div><!-- /container -->
</header>
