<?php
/**
 * Content Template File for Login Content Section
 *
 * @project J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright 2008-2017 by Jan Doll
 */

use JFrame\Theming\ThemeGlobals;

/** @var ThemeGlobals $globals */
?>
<canvas id="particles"></canvas>
<div class="container main-content--wrapper">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 center-vertical login-wrapper">
            <div class="main-content">
                <h1 class="page-header"><?php echo $globals->getTitle(); ?></h1>
                <?php echo $globals->getContent(); ?>
            </div>
        </div>
    </div>
</div>
