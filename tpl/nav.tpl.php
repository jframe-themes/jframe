<?php
/**
 * Nav Template File for Navigation Section
 *
 * @project J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright 2008-2017 by Jan Doll
 */

use JFrame\Theming\Navigation\Navigation;
use JFrame\Theming\Navigation\NavigationItem;
use JFrame\Theming\Navigation\NavigationManager;
use JFrame\Theming\ThemeGlobals;

/** @var ThemeGlobals $globals */

//$accClass = $Mvc->modelClass('Acc');
//$userMenu = (is_object($accClass)) ? $accClass->getUserMenu() : '';
$userMenu = $globals->get('user-menu') ?? '';

//$projectName = $this->Core->Config()->get('project_name');
$projectName = '';

//$site     = $this->Core->Sites()->getSite();
//$siteName = (count($site)) ? $site['name'] : '';
$siteName = '';

$navbarBrand = ($siteName != '') ? $projectName . ' - ' . $siteName : $projectName;

$navHtml = '';
$mainNavigation = $globals->getNavigation(NavigationManager::MAIN_NAVIGATION_KEY);
$mainNavigationItems = ($mainNavigation instanceof Navigation) ? $mainNavigation->getItems() : [];
$navItems = $navItems ?? $mainNavigationItems;

/** @var NavigationItem $navElement */
foreach ($navItems as $navElement) {
    $navHtml .= buildNavElementHtml($navElement, $globals);
}

?>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Navigation ein-/ausblenden</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand visible-xs one-line-ellipsis" href="<?php echo $globals->getBaseUrl(); ?>" title="<?php echo $navbarBrand; ?>"><?php echo $navbarBrand; ?></a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <?php echo $navHtml; ?>
            </ul>

            <?php if($userMenu !== ''): ?>
            <ul class="nav navbar-nav navbar-right hidden-xs">
                <?php echo $userMenu; ?>
            </ul>
            <?php endif; ?>
        </div>
    </div>
</nav>

<div class="container visible-xs">
    <?php // echo $Mvc->getBreadcrumb(); ?>
</div>
