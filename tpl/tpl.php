<?php
/**
 * Main Template File to include all Sections
 *
 * @project J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright 2008-2017 by Jan Doll
 */

/** @var JFrame\Theming\ThemeGlobals $globals */

use JFrame\Configuration\Exception\ConfigurationNotFound;
use JFrame\Theme\JFrame\Configuration\ThemeJFrameConfigurationKeys;
use JFrame\Theming\Navigation\Navigation;
use JFrame\Theming\Navigation\NavigationItem;
use JFrame\Routing\Route\Route;

$cfg = $globals->getConfig();
$tplPath = static function (string $tplFileName) use ($cfg): string
{
    $defaultTplPath = rtrim(JF_THEMES_DIR,DS) . DS . 'jframe' . DS . 'tpl';
    try {
        $configTplPAth = $cfg->get(ThemeJFrameConfigurationKeys::TPL_PATH);
    } catch (ConfigurationNotFound $e) {
        $configTplPAth = $defaultTplPath;
    }

    $useTplPath = rtrim($defaultTplPath, DS) . DS . $tplFileName;
    if($configTplPAth !== $defaultTplPath && file_exists(rtrim($configTplPAth, DS) . DS . $tplFileName)) {
        $useTplPath = rtrim($configTplPAth, DS) . DS . $tplFileName;
    }

    return $useTplPath;
};

/** @var Route $currentRoute */
$currentRoute = $globals->getRoute();

$isLoginPage = ($currentRoute instanceof Route && strpos($currentRoute->getName(), '_login') !== false);

function buildNavElementHtml(NavigationItem $navigationItem, JFrame\Theming\ThemeGlobals $globals): string
{
    $navElementHtml = '';

    $active = ($navigationItem->isActive()) ? 'active' : '';

    $dropdownPrefix = ($active !== '') ? ' ': '';

    $hasChildren = (count($navigationItem->getChildren()));
    $dropdown = ($hasChildren) ? $dropdownPrefix . 'dropdown' : '';

    $liClass = ($active !== '' || $dropdown !== '') ? ' class="' . $active . $dropdown . '"' : '';
    $aAttributes = ($hasChildren) ? ' class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-key="' . $navigationItem->getKey() . '"' : ' data-key="' . $navigationItem->getKey() . '"';
    $aHref = ($hasChildren) ? '#' : $navigationItem->getHref();
    $aTarget = ($navigationItem->getTarget() !== NavigationItem::TARGET_SELF) ? ' target="' . $navigationItem->getTarget() . '"' : '';
    $aCaret = ($hasChildren) ? ' <span class="caret"></span>' : '';
    $itemName = $globals->getTranslator()->translate($navigationItem->getName());
    $itemType = $navigationItem->getType();

    if ($itemType === NavigationItem::TYPE_SEPARATOR) {
        $navElementHtml .= '<li role="separator" class="divider" data-pos="' . $navigationItem->getPosition() . '" data-children="' . (($hasChildren) ? '1' : '0') . '" data-type="' . $itemType . '"></li>' . PHP_EOL;
    } else {
        $navElementHtml .= '<li' . $liClass . ' data-pos="' . $navigationItem->getPosition() . '" data-children="' . (($hasChildren) ? '1' : '0') . '" data-type="' . $itemType . '">' . PHP_EOL;
    }

    if ($itemType === NavigationItem::TYPE_LINK || ($itemType === NavigationItem::TYPE_TEXT && $hasChildren)) {
        $navElementHtml .= '<a href="' . $aHref . '"' . $aTarget . $aAttributes . $aTarget . '>' . PHP_EOL;
        $navElementHtml .= $itemName . $aCaret . PHP_EOL;
        $navElementHtml .= '</a>' . PHP_EOL;
    }

    if ($itemType === NavigationItem::TYPE_TEXT && !$hasChildren) {
        $navElementHtml .= '<span class="navbar-text">' . PHP_EOL;
        $navElementHtml .= $itemName . PHP_EOL;
        $navElementHtml .= '</span>' . PHP_EOL;
    }

    if ($hasChildren && $itemType !== NavigationItem::TYPE_SEPARATOR) {
        $navElementHtml .= '<ul class="dropdown-menu">' . PHP_EOL;
        foreach ($navigationItem->getChildren() as $child) {
            $navElementHtml .= buildNavElementHtml($child, $globals);
        }
        $navElementHtml .= '</ul>' . PHP_EOL;
    }

    if ($itemType !== NavigationItem::TYPE_SEPARATOR) {
        $navElementHtml .= '</li>' . PHP_EOL;
    }

    return $navElementHtml;
}

$userMenu = '';
$userNavigation = $globals->getNavigation('user');
$userNavigationItems = ($userNavigation instanceof Navigation) ? $userNavigation->getItems() : [];

/** @var NavigationItem $navElement */
foreach ($userNavigationItems as $navElement) {
    $userMenu .= buildNavElementHtml($navElement, $globals);
}

$globals->add('user-menu', $userMenu);

$bodyClassAttr = (($bodyClasses = trim($globals->getBodyClasses())) !== '') ? ' class="' . $bodyClasses . '"' : '';
?><!DOCTYPE html>
<html lang="<?php echo $globals->getLanguage()->getCode(); ?>">
    <head>
        <?php include($tplPath('head.tpl.php')); ?>
    </head>
    <body<?php echo $bodyClassAttr; ?>>
        <?php echo $globals->getAfterBodyStart(); ?>
        <?php
            include($tplPath('header.tpl.php'));
            if($isLoginPage){
                include($tplPath('content.login.tpl.php'));
            } else {
                include($tplPath('nav.tpl.php'));
                include($tplPath('content.tpl.php'));
            }
            include($tplPath('footer.tpl.php'));
        ?>
    </body>
</html>

