<?php
/**
 * Footer Template File do set Javascript Files etc.
 *
 * @project J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright 2008-2017 by Jan Doll
 */

use JFrame\Theme\JFrame\Configuration\ThemeJFrameConfigurationKeys as ConfigKey;
use JFrame\Theming\ThemeGlobals;

/** @var ThemeGlobals $globals */

?>
<footer class="footer">
    <div class="container">
        <p class="text-muted copyright"><?php echo $globals->getConfig()->get(ConfigKey::FOOTER_COPY); ?></p>
    </div>
</footer>

<?php echo $globals->getBeforeBodyEnds(); ?>
<script src="<?php echo $globals->getConfig()->get(ConfigKey::JQUERY_CDN); ?>"></script>
<script src="<?php echo $globals->getBaseUrl(); ?>composer/vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo $globals->getSkinUri(); ?>/lib/particles/particles.min.js"></script>
<script src="<?php echo $globals->getSkinUri(); ?>/js/main.js"></script>
