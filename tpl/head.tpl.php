<?php
/**
 * Head Template File for Meta Section
 *
 * @project J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright 2008-2017 by Jan Doll
 */

use JFrame\Theming\ThemeGlobals;

/** @var ThemeGlobals $globals */

?>
<meta charset="utf-8">
<meta name="robots" content="noindex, nofollow">
<title><?php echo $globals->getMetaTitle(); ?></title>
<meta name="description" content="<?php echo $globals->getMetaDescription(); ?>"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="<?php echo $globals->getSkinUri(); ?>/css/styles.css.php">
<link rel="shortcut icon" href="<?php echo $globals->getSkinUri(); ?>/img/favicon.ico" type="image/vnd.microsoft.icon"/>
<?php echo $globals->getHeadMeta(); ?>
