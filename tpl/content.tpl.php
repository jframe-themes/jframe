<?php
/**
 * Content Template File for Main Content Section
 *
 * @project J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright 2008-2017 by Jan Doll
 */

use JFrame\Theming\ThemeGlobals;

/** @var ThemeGlobals $globals */
?>
<div class="container main-content--wrapper">
    <div class="main-content">
        <h1 class="page-header"><?php echo $globals->getTitle(); ?></h1>
        <?php echo $globals->getContent(); ?>
    </div>
</div>
