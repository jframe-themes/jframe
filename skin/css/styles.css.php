<?php
/**
 * CSS Styles Less Compiler CSS Output
 *
 * @project J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright 2008-2017 by Jan Doll
 */

use JFrame\Caching\Exception\CacheItemNotFoundException;use JFrame\Caching\Exception\InvalidArgumentException;use JFrame\Caching\Exception\InvalidArgumentExceptionInterface;use JFrame\Caching\FileCacheAdapter;use JFrame\Caching\Pool\FileCacheItem;use JFrame\Configuration\Config;
use JFrame\Theme\JFrame\Configuration\ThemeJFrameConfigurationKeys;

header("Content-type: text/css", true);

$seconds_to_cache = 3600;

$ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
header("Expires: $ts");
header("Pragma: cache");
header("Cache-Control: max-age=$seconds_to_cache");

$ds = DIRECTORY_SEPARATOR;
$rootPath = str_replace($ds . 'JFrame' . $ds . 'Themes' . $ds . 'jframe' . $ds . 'skin' . $ds . 'css', '', __DIR__);
$appPath = $rootPath . $ds . 'JFrame';

/** @noinspection PhpIncludeInspection */
require_once $rootPath . $ds . 'composer' . $ds . 'vendor' . $ds . 'wikimedia' . $ds . 'less.php' . $ds . 'lib' . $ds . 'Less' . $ds . 'Autoloader.php';
Less_Autoloader::register();

$JFrame = null;
ob_start(); // prevent output
/** @noinspection PhpIncludeInspection */
require_once $appPath . $ds . 'init.php';
ob_get_clean();

/** @noinspection PhpExpressionAlwaysNullInspection */
/** @var FileCacheAdapter $cache */
$cache = $JFrame->getDependencyResolver()->resolve(FileCacheAdapter::class);

$cssCacheKey = 'jframe_theme_css';

try {
    $cssCache = $cache->getItem($cssCacheKey);
} catch (CacheItemNotFoundException | InvalidArgumentExceptionInterface $e) {
    $cssCache = null;
}

$css = '';
if ($cssCache instanceof FileCacheItem) {
    $css = $cssCache->get();
}

if ($css === '') {
    /** @noinspection PhpExpressionAlwaysNullInspection */
    /** @var Config $config */
    $config = $JFrame->getDependencyResolver()->resolve(Config::class);

    $configSchemaName = $config->get(ThemeJFrameConfigurationKeys::SCHEMA_NAME);
    $configSchemaPath = $config->get(ThemeJFrameConfigurationKeys::SCHEMA_PATH);
    $configAddCss     = $config->get(ThemeJFrameConfigurationKeys::ADDITIONAL_CSS);

    $less = new Less_Parser();

    $schemas = [
        'light',
        'dark',
        'desert',
        'blue'
    ];

    $getSchema = (isset($_GET['schema']) && $_GET['schema'] !== '') ? $_GET['schema'] : '';
    $schema = (in_array($getSchema, $schemas, true)) ? $getSchema : 'light';

    $schema = ($schema === 'blue') ? 'sky' : $schema;

    if (file_exists($configSchemaPath . $ds . 'colors.less')){
        $less->parseFile($configSchemaPath . $ds . 'colors.less');
    } else {
        $defaultSchemaPath = rtrim(JF_THEMES_DIR,$ds) . $ds . 'jframe' . $ds . 'skin' . $ds . 'less' . $ds . 'variables';
        $less->parseFile($defaultSchemaPath . $ds . $schema . $ds . 'colors.less');
    }

    $themeSkinPath = $rootPath . $ds . 'JFrame' . $ds . 'Themes' . $ds . 'jframe' . $ds . 'skin';
    $less->parseFile($themeSkinPath . $ds . 'less' . $ds . 'styles.less');

    $less->SetOption('compress', true);

    $css = $less->getCss();

    $css .= $configAddCss;

    try {
        $cache->saveToCache($cssCacheKey, $css);
    } catch (InvalidArgumentException $e) {
        // die silently
    }
}

echo $css;
