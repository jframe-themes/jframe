jQuery( document ).ready(function($) {
	let $body = $('body');

	/** Popover / Tooltip Init */
	$body.popover({
		selector: '[data-toggle="popover"]',
		html: true,
		container: 'body',
		viewport: 'body'
	});

	$body.tooltip({
		selector: '[data-toggle="tooltip"]',
		html: true,
		container: 'body',
		viewport: 'body'
	});

	if($('#particles').length && typeof Particles !== 'undefined') {
		Particles.init({
			selector: '#particles',
		  	color: '#9c27b0',
	  		connectParticles: true
		});
	}
});
